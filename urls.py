import os
import requests


def seq(line):
    for i in line.split(','):
        print(requests.get(i).text)


def main():
    seq(os.environ["urls"])


if __name__ == '__main__':
    main()

