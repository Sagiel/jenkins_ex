import os


def seq(num):
    for i in range(int(num)):
        print(i, i**2)


def main():
    num = os.environ["number"]
    seq(num)
#    if len(sys.argv) < 2:
#       exit("No file path was entered")
#    elif len(sys.argv) > 2:
#        exit("More than one file path was entered")
#    else:
#        seq(sys.argv[1])


if __name__ == '__main__':
    main()
